#include <cstdio>
#include <ctime>
#include <cmath>

int main(int argc, char* argv[]) {
  float p,q,r,v,w,x,fw,fx,fv,t1,t2,t3,t4,t5,t6;

  v = 1.12345;
  w = v;
  x = v;
  fv = v;
  fw = v;
  fx = v;

  t1 = clock();
  for(int i = 1000000; i--;) {
    r=(x-w)*(fx-fv);
    q=(x-v)*(fx-fw);
    p=(x-v)*q-(x-w)*r;
    q=2.0*(q-r);
  }
  t2 = clock();

  v = 1.12345;
  w = v;
  x = v;
  fv = v;
  fw = v;
  fx = v;

  t3 = clock();
  float a,b;
  for(int i = 1000000; i--;) {
    a = (x-w);
    b = (x-v);
    r=a*(fx-fv);
    q=b*(fx-fw);
    p=b*q-a*r;
    q=2.0*(q-r);
  }
  t4 = clock();

  v = 1.12345;
  w = v;
  x = v;
  fv = v;
  fw = v;
  fx = v;

  t5 = clock();
  for(int i = 1000000; i--;) {
    q=(x-v)*(fx-fw);
    p=(w-v)*q-(x-w)*((x-w)*(fx-fv));
    q=2.0*(q-((x-w)*(fx-fv)));
  }
  t6 = clock();

  printf("T1 %f T2 %f r=%f D=%f\n", t1, t2, r, (t2-t1));
  printf("T3 %f T4 %f r=%f D=%f\n", t3, t4, r, (t4-t3));
  printf("T5 %f T6 %f r=%f D=%f\n", t5, t6, r, (t6-t5));
}
