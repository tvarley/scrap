#!/usr/bin/env ruby

require 'awesome_print'
require 'byebug'
require 'nokogiri'
require 'active_support/core_ext/hash/conversions'
require 'json'

def good_xml
  <<-FOO
  <xml>
    <foo>
      <number>1</number>
    </foo>
    <foo>
      <number>2</number>
    </foo>
  </xml>
  FOO
end

def bad_xml
  <<-FOO
  <xml>
    <foo>
      <number>1</number>
    </foo>
  </xml>
  FOO
end

def tag_arrays(xml)
  noko_doc = Nokogiri::XML(xml)
  [
    "//xml/foo"
  ].each do |xpath|
    noko_doc.xpath(xpath).each do |node|
      node.name = "array_#{node.name}"
    end
  end
  noko_doc.to_s
end

def untag_hash(hash)
  ret = {}
  hash.each do |key, value|
    new_key = key
    new_value = value
    if key.start_with?('array_')
      new_key = key.split('_')[1]
      new_value = [value] unless value.is_a?(Array)
    end
    ret[new_key] = new_value
    if new_value.is_a?(Hash)
      ret[new_key] = untag_hash(new_value)
    end
  end
  ret
end

def show(xml, title)
  p '-----------------'
  p "#{title}"
  p 'Original XML'
  ap xml
  p 'Hash from XML (normal)'
  xml_hash = Hash.from_xml(xml)
  ap xml_hash
  p 'Hash from XML (tagged)'
  tagged_xml_hash = Hash.from_xml(tag_arrays(xml))
  ap tagged_xml_hash
  p 'Hash from JSON'
  json_hash = JSON.parse(xml_hash.to_json)
  ap json_hash
  p 'Hash from tagged JSON'
  json_tagged_hash = untag_hash(JSON.parse(tagged_xml_hash.to_json))
  ap json_tagged_hash
  p '-----------------'
end

show(bad_xml, 'Bad XML')
show(good_xml, 'Good XML')

ap good_xml
ap bad_xml


