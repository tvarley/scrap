#!/usr/bin/env ruby

def test1(fred:)
  [ fred, fred* 2]
end

a = []
b = []

(1..10).each do |i|
  a_x,b_x = test1(fred: i)
  a << a_x
  b << b_x
end

puts a
puts b
